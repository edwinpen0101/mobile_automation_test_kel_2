@Login
Feature: Login Feature

   
  	Scenario: user can successfully login
		Given user in login page
		When user input valid email and valid password in login page
		And user click login button
		Then user successfuly login 
		
		Scenario Outline: user can not successfully login due <condition>
		Given user in login page
		When user input <condition> in login page
		And user click login button
		Then user can not login due <condition> 
    
    Examples: 
    
      | test_case_id | condition                   			 |
      | TC Login 2	 | invalid email and valid password  |
      | TC login 3   | empty email and valid password	   |
      | TC login 4	 | valid email and invalid password  |
      | TC login 5	 | valid email and empty password  	 |