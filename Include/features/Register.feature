@Register
Feature: Register
 Scenario Outline: user register an account <condition>
    Given user open secondhad apps
  	When user tap akun on main page
  	Then user tap masuk on akun page
    Then user tap daftar on login page
   	Then user fill <condition> in register page
    And user tap daftar
    Then user <result> register an account




	Examples: 
	|case_id	| condition											| result	|
	|TC_001	| valid data											| success	|
	|TC_002	| empty name 											| fail1		|
	|TC_003	| empty email											| fail2		|
	|TC_004	| invalid format email						| fail3		|
	|TC_005	| existing email									| fail4		|
	