@Profile_User
Feature: Profile
  User want update profile data

  Scenario Outline: User <expect> update data profile 
    Given user in home page
    When User click profile
    #Then User redirect profile page
    #And User set Avatar
    And User input Name <name>
    And User input Kota <kota>
    And User input Alamat <alamat>
    And User input Hanphone <no_hp>
    Then User <expect> update profile <test_case_id>

    Examples: 
      |test_case_id		| name 				 | kota    				 |  alamat  					| no_hp      | expect  |
      |TC.Profile.001	| edwin  			 | Yogyakarta			 |  Jalan sawah besar | 0812387192 | success |
      |TC.Profile.002 |       			 | Malang 		 		 |  Jalan sawah kecil | 0812387192 | fail 	 |
      |TC.Profile.003 | \s  			 	 | Yogyakarta			 |  Jalan sawah besar | 0812387192 | fail 	 |
      |TC.Profile.004 | edwin  			 | 				         |  Jalan sawah besar | 0812387192 | fail		 |
      |TC.Profile.005 | sukoco  		 | Yogyakarta			 |  Jalan sawah besar | %4adbasd   | fail		 |