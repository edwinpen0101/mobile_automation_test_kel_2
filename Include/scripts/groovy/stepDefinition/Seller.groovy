package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.security.PublicKey

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.Given
import cucumber.api.java.en.When
import cucumber.api.java.en.Then
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.AppiumDriver
import com.kms.katalon.core.util.KeywordUtil


public class Seller {
	@Given("user in homepage")
	public void user_in_homepage() {

		Mobile.startApplication('Application/secondhand-24082023.apk', true)

		Mobile.waitForElementPresent(findTestObject('Object Repository/Page_product/button_akun'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/button_akun'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/B_masuk'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/input_email'), 0)

		Mobile.setText(findTestObject('Object Repository/Page_product/input_email'), 'i2fj02hdwj_ifewhj@gmail.com', 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/input_password'), 0)

		Mobile.setText(findTestObject('Object Repository/Page_product/input_password'), '123456', 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/button_masuk'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/button_masuk'), 0)
	}

	@When("user click jual")
	public void user_click_jual() {

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/jual'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/jual'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Nama_Produk'), 0)

		Mobile.setText(findTestObject('Object Repository/Page_product/Nama_Produk'), 'AC Loewad', 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/harga'), 0)

		Mobile.setText(findTestObject('Object Repository/Page_product/harga'), '18901', 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Lokasi Produk'), 0)

		Mobile.setText(findTestObject('Object Repository/Page_product/Lokasi Produk'), 'Jakarta Barat', 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/button_foto_produk'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/button_foto_produk'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/galeri'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/galeri'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/gambar1'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/gambar1'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Pilih Kategori'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Pilih Kategori'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/elektronik'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/elektronik'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Deskripsi'), 0)

		Mobile.setText(findTestObject('Object Repository/Page_product/Deskripsi'), '2PK', 0)
	}

	@Then("user successfully add product correctly")
	public void user_successfully_add_product_correctly() {

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Terbitkan'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Terbitkan'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Button_Back (1)'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Button_Back (1)'), 0)

		

		Mobile.tap(findTestObject('Object Repository/Page_product/Daftar_Jual_Saya'), 0)
	}

	@When("user fill with minus price")
	public void user_fill_with_minus_price() {

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/jual'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/jual'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Nama_Produk'), 0)

		Mobile.setText(findTestObject('Object Repository/Page_product/Nama_Produk'), 'SmartTV Android bisa netplik', 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/harga'), 0)

		Mobile.setText(findTestObject('Object Repository/Page_product/harga'), '-123456', 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Lokasi Produk'), 0)

		Mobile.setText(findTestObject('Object Repository/Page_product/Lokasi Produk'), 'Jakarta Pusat', 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/button_foto_produk'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/button_foto_produk'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/galeri'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/galeri'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/gambar1'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/gambar1'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Pilih Kategori'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Pilih Kategori'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/elektronik'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/elektronik'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Deskripsi'), 0)

		Mobile.setText(findTestObject('Object Repository/Page_product/Deskripsi'), 'mesin cuci 10kg', 0)
	}

	@Then("user successfully add product with minus price")
	public void user_successfully_add_product_with_minus_price() {

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Terbitkan'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Terbitkan'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Button_Back (1)'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Button_Back (1)'), 0)

		

		Mobile.tap(findTestObject('Object Repository/Page_product/Daftar_Jual_Saya'), 0)
	}

	@When("user click product")
	public void user_click_product() {

		

		Mobile.tap(findTestObject('Object Repository/Page_product/Daftar_Jual_Saya'), 0)
	}

	@Then("user successfully delete product")
	public void user_want_successfully_delete_product() {

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Delete'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Delete'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Hapus'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Hapus'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Button_Back (1)'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Button_Back (1)'), 0)

		

		Mobile.tap(findTestObject('Object Repository/Page_product/Daftar_Jual_Saya'), 0)
	}

	@When("user click daftar jual saya")
	public void userclickdaftarjualsaya() {

		

		Mobile.tap(findTestObject('Object Repository/Page_product/Daftar_Jual_Saya'), 0)
	}

	@Then('user successfully see own product')
	public void user_successfully_see_own_product() {

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Terjual'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Terjual'), 1)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Diminati'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Diminati'), 1)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Produk'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Produk'), 1)
	}

	@Then("user successfully change product")
	public void user_successfully_change_product() {

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Product_1'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Product_1'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Nama_Produk'), 0)

		Mobile.setText(findTestObject('Object Repository/Page_product/Nama_Produk'), 'Air Purifier', 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/harga'), 0)

		Mobile.setText(findTestObject('Object Repository/Page_product/harga'), '777777', 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Lokasi Produk'), 0)

		Mobile.setText(findTestObject('Object Repository/Page_product/Lokasi Produk'), 'Jakarta Timur', 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/button_foto_produk'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/button_foto_produk'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/galeri'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/galeri'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/gambar1'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/gambar1'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Pilih Kategori'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Pilih Kategori'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/elektronik'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/elektronik'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Terbitkan'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Terbitkan'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Product_1'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Product_1'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Button_Back (1)'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Button_Back (1)'), 0)

		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_product/Button_Back (1)'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_product/Button_Back (1)'), 0)

		

		Mobile.tap(findTestObject('Object Repository/Page_product/Daftar_Jual_Saya'), 0)
	}
}
