package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovy.json.StringEscapeUtils

public class Login {
	@Given("user in login page")
	public void user_is_in_login_page () {
		Mobile.startApplication('Application/secondhand-24082023.apk', true)
		Mobile.tap(findTestObject('Object Repository/Page_login/BTN_Akun'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_login/btn_Masuk'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_login/btn_Masuk'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_login/input email'), 0)
	}

	@When("user input (.*) in login page")
	public void user_input_in_login_page (String condition) {
		if (condition=="valid email and valid password") {
			Mobile.setText(findTestObject('Object Repository/Page_login/input email'), 'work1223@gmail.com', 0)
			Mobile.setText(findTestObject('Object Repository/Page_login/input password'), '123456', 0)
		}

		if (condition=="invalid email and valid password") {
			Mobile.setText(findTestObject('Object Repository/Page_login/input email'), 'work122391@gmail.com', 0)
			Mobile.setText(findTestObject('Object Repository/Page_login/input password'), '123456', 0)
		}

		if (condition=="empty email and valid password") {
			Mobile.setText(findTestObject('Object Repository/Page_login/input email'), '', 0)
			Mobile.setText(findTestObject('Object Repository/Page_login/input password'), '123456', 0)
		}

		if (condition=="valid email and invalid password") {
			Mobile.setText(findTestObject('Object Repository/Page_login/input email'), 'work1223@gmail.com', 0)
			Mobile.setText(findTestObject('Object Repository/Page_login/input password'), '123426', 0)
		}

		if (condition=="valid email and empty password") {
			Mobile.setText(findTestObject('Object Repository/Page_login/input email'), 'work1223@gmail.com', 0)
			Mobile.setText(findTestObject('Object Repository/Page_login/input password'), '', 0)
		}
	}
	@When("user click login button")
	public void user_click_login_button () {
		Mobile.tap(findTestObject('Object Repository/Page_login/Btn_masuk_login page'), 0)
	}

	@Then("user successfuly login")
	public void user_successfuly_login () {
		Mobile.verifyElementVisible(findTestObject('Object Repository/page_my_account/btn_logout'), 0)
	}

	@Then("user can not login due (.*)")
	public void user_can_not_login (String condition) {
		if (condition=="invalid email and valid password")
			Mobile.verifyElementVisible(findTestObject('Object Repository/Page_login/Btn_masuk_login page'), 0)

		if (condition=="empty email and valid password")
			Mobile.verifyElementVisible(findTestObject('Object Repository/Page_login/warning_email tidak boleh kosong'), 0)

		if (condition=="valid email and invalid password")
			Mobile.verifyElementVisible(findTestObject('Object Repository/Page_login/Btn_masuk_login page'), 0)

		if (condition=="valid email and empty password")
			Mobile.verifyElementVisible(findTestObject('Object Repository/Page_login/warning_password tidak boleh kosong'), 0)
	}
}
