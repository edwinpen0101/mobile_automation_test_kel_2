package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.AppiumDriver

import com.kms.katalon.core.util.KeywordUtil

class Register {
	@Given("user open secondhad apps")
	public void  user_open_secondhad_apps( ) {
		Mobile.startApplication('Application/secondhand-24082023.apk', true)
		Mobile.waitForElementPresent(findTestObject('Object Repository/Page_secondhand/btn_Akun'), 0)
	}
	@When("user tap akun on main page")
	public void  user_tap_akun_on_main_page( ) {
		Mobile.tap(findTestObject('Object Repository/Page_secondhand/btn_Akun'), 0)
	}
	@Then("user tap masuk on akun page")
	public void  user_tap_masuk_on_akun_page( ) {
		Mobile.tap(findTestObject('Object Repository/Page_akun/btn_masuk_akun'), 0)
	}
	@Then("user tap daftar on login page")
	public void  user_tap_daftar_on_login_page( ) {
		Mobile.tap(findTestObject('Object Repository/Page_login/btn_daftar_login'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_register/input_alamat'), 0)
	}
	@Then("user fill (.*) in register page")
	public void  user_fill_in_register_page(String condition ) {
		if(condition=="valid data") {
			Mobile.setText(findTestObject('Object Repository/Page_register/input_nama'), 'doni', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_email'), 'doni91@test.com', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_password'), '123456789', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_noHP'), '08124563578', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_kota'), 'bali', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_alamat'), 'jalan sehat no 21', 0)
		}
		else if(condition=="empty name") {
			//			Mobile.setText(findTestObject('Object Repository/Page_register/input_nama'), 'doni', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_email'), 'dsq23wq@test.com', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_password'), '123456789', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_noHP'), '08124563578', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_kota'), 'bali', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_alamat'), 'jalan sehat no 21', 0)
		}
		else if(condition=="empty email") {
			Mobile.setText(findTestObject('Object Repository/Page_register/input_nama'), 'doni', 0)
			//			Mobile.setText(findTestObject('Object Repository/Page_register/input_email'), 'doni@test.com', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_password'), '123456789', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_noHP'), '08124563578', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_kota'), 'bali', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_alamat'), 'jalan sehat no 21', 0)
		}
		else if(condition=="invalid format email") {
			Mobile.setText(findTestObject('Object Repository/Page_register/input_nama'), 'doni', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_email'), 'doni.test.com', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_password'), '123456789', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_noHP'), '08124563578', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_kota'), 'bali', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_alamat'), 'jalan sehat no 21', 0)
		}
		else if(condition=="existing email") {
			Mobile.setText(findTestObject('Object Repository/Page_register/input_nama'), 'doni', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_email'), 'doni@test.com', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_password'), '123456789', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_noHP'), '08124563578', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_kota'), 'bali', 0)
			Mobile.setText(findTestObject('Object Repository/Page_register/input_alamat'), 'jalan sehat no 21', 0)
		}
	}
	@When("user tap daftar")
	public void  user_tap_daftar( ) {
		Mobile.tap(findTestObject('Object Repository/Page_register/btn_daftar'), 0)
	}
	@Then("user(.*) register an account")
	public void  user_register_an_account (String result){
		AppiumDriver<?> driver = MobileDriverFactory.getDriver()
		if(result=="success") {
			Mobile.verifyElementVisible(findTestObject('Object Repository/page_my_account/btn_logout'), 0)
		}	else if(result=="fail1") {
			Mobile.verifyElementVisible(findTestObject('Object Repository/Page_register/warningtext_nama'), 0)
		}
		else if(result=="fail2") {
			Mobile.verifyElementVisible(findTestObject('Object Repository/Page_register/warningtext_email'), 0)
		}
		else if(result=="fail3") {
			Mobile.verifyElementVisible(findTestObject('Object Repository/Page_register/warninginvalid_email'), 0)
		}
		else if(result=="fail4") {
			driver.findElementByXPath("//android.widget.Toast[@text='Email sudah digunakan']")
		}
	}
}